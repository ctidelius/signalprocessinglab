package com.example.android.signallab;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.media.audiofx.AcousticEchoCanceler;
import android.media.audiofx.Visualizer;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.gauravk.audiovisualizer.visualizer.BarVisualizer;
import com.gauravk.audiovisualizer.visualizer.WaveVisualizer;

import java.nio.ByteBuffer;

/*
* AudioManager provides access to the volume and ringer
* AudioRecord "pulls" or reads data into an array with the read() method.
* AudioTrack manages and plays a single audio resource for Java applications.
* It allows streaming of PCM audio buffers to the audio sink for playback.
* This is achieved by "pushing" the data to the AudioTrack object using one of the write() methods
 */
public class RecorderActivity extends AppCompatActivity {
    private boolean isRecording = false;
    private AudioManager am = null;
    private AudioRecord record = null;
    private AudioTrack track = null;
    private int sampleRateHz = 8000;
    private Button startRecording, stopRecording;
    BarVisualizer mViz;
    byte[] audioBuffer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorder);
        setVolumeControlStream(AudioManager.MODE_IN_COMMUNICATION);
        /* Initializing AudioRecord and AudioTrack, and trying to do an EchoCanceller, if available */
        initializeRecordAndTrack();
        mViz = findViewById(R.id.bar);
        am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        am.setSpeakerphoneOn(true);
        /*
        * Start a new thread to run recordAndPlay to not interfere with UI-thread.
        * Since we are probably going to do some visualization on the UI-thread, it cannot be stopped
        * by the record and play which takes up some of the processing power.
        */
        (new Thread()
        {
            @Override
            public void run()
            {
                recordAndPlay();
            }
        }).start();

        startRecording = findViewById(R.id.record);
        startRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isRecording)
                {
                    startRecordingAndPlaying();
                }
            }
        });
        stopRecording = findViewById(R.id.stop);
        stopRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isRecording)
                {
                    stopRecordingAndPlaying();
                }
            }
        });

    }
    /*
    * Records audio with read(), into vizBuffer, with a maxiumum length of 1024 byte-values.
    * Then plays back the audio with write, from audioBuffer with a length of how many bytes
    * were read into the array.
    * num = the number of bytes read into the audioBuffer, zero, or an error code
    *
     */
    private void recordAndPlay()
    {
        audioBuffer = new byte[1024];
        int num;
        am.setMode(AudioManager.MODE_IN_COMMUNICATION);
        while (true)
        {
            if (isRecording)
            {
                /*Reading in the values from the microphone to the vizbuffer, which is 1024 bytes long*/
                num = record.read(audioBuffer, 0,1024);

                /* TODO manipulate the audioBuffer array */

                /*Writing the vizBuffer to the AudioTrack to be able to play it*/
                track.write(audioBuffer, 0, num);

                try {
                    /* Have to do graphical updates on the main thread (UI thread)*/
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mViz.setRawAudioBytes(audioBuffer);

                        }
                    });
                    Thread.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
        }
    }
    /*
    * Starts recording audio and plays the the audio that has been written to the AudioTrack
     */
    private void startRecordingAndPlaying()
    {
        record.startRecording();
        track.play();
        isRecording = true;

    }
    /*
    * Stops recording and pauses the audio being played
     */
    private void stopRecordingAndPlaying()
    {
        record.stop();
        track.pause();
        isRecording = false;
    }

    /*
    * Initializes the AudioRecord and AudioTrack.
    * Uses the sampleRate specified before, Format in Mono. Encoding in 16 bits.
    * getMinBufferSize returns the minimum buffer size required for the successful creation of an
    * AudioRecord object, in byte units.
     */
    private void initializeRecordAndTrack(){

        int minBufferSize = AudioRecord.getMinBufferSize(sampleRateHz,
                AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        record = new AudioRecord(MediaRecorder.AudioSource.MIC, sampleRateHz,
                AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, minBufferSize);
        if (AcousticEchoCanceler.isAvailable())
        {
            AcousticEchoCanceler echoCanceler = AcousticEchoCanceler.create(record.getAudioSessionId());
            echoCanceler.setEnabled(true);
        }
        int maxJitter = AudioTrack.getMinBufferSize(sampleRateHz,
                AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
        track = new AudioTrack(AudioManager.MODE_IN_COMMUNICATION, sampleRateHz,
                AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, maxJitter,
                AudioTrack.MODE_STREAM);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        record.release();
        track.release();
        if (mViz != null)
            mViz.release();
    }
    @Override
    protected void onPause() {
        super.onPause();
        record.release();
        track.release();
        if (mViz != null)
            mViz.release();
    }
    @Override
    protected void onResume() {
        super.onResume();
        initializeRecordAndTrack();
        mViz = findViewById(R.id.bar);
    }

}
